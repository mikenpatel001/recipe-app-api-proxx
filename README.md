# Recie App API Proxy


NGINX proxy app for our recipe app API

## Usage

### Environment variables

* 'LISTEN PORT'-Port to listen on(default 8000)
* 'APP_HOST'-Hostname of the app to forward request to (default:app)
* 'APP_PORT'-Port of the app to forward request to (default:9000)


